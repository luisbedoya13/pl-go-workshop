package ch03

import "fmt"

func LoopingOverString() {
	logLevel := "デバッグ"
	for index, runeVal := range logLevel {
		fmt.Println(index, string(runeVal))
	}
}
