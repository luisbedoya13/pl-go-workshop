package ch14

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
)

type server struct{}
type messageData struct {
	Message string `json:"message"`
}

func (srv server) ReadJSONFromRqAndReply(
	w http.ResponseWriter, r *http.Request,
) {
	jsonDecoder := json.NewDecoder(r.Body)
	messageData := messageData{}
	err := jsonDecoder.Decode(&messageData)
	if err != nil {
		log.Fatal(err)
	}
	jsonBytes, _ := json.Marshal(messageData)
	log.Println(string(jsonBytes))
	w.Write(jsonBytes)
}

func PostDataAndReturnRs(msg messageData) messageData {
	jsonBytes, _ := json.Marshal(msg)
	r, err := http.Post(
		"http://localhost:8080",
		"application/json",
		bytes.NewBuffer(jsonBytes),
	)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	message := messageData{}
	err = json.Unmarshal(data, &message)
	if err != nil {
		log.Fatal(err)
	}
	return message
}

func (srv server) ReadFileFromRq(
	rs http.ResponseWriter,
	rq *http.Request,
) {
	uploadedFile, uploadedFileHeader, err := rq.FormFile("myFile")
	if err != nil {
		log.Fatal(err)
	}
	defer uploadedFile.Close()
	fileContent, err := ioutil.ReadAll(uploadedFile)
	if err != nil {
		log.Fatal(err)
	}
	err = ioutil.WriteFile(
		fmt.Sprintf("./%s", uploadedFileHeader.Filename),
		fileContent,
		0666,
	)
	if err != nil {
		log.Fatal(err)
	}
	rs.Write([]byte(
		fmt.Sprintf("%s Uploaded!", uploadedFileHeader.Filename),
	))
}

func PostFileAndReturnRs(filename string) string {
	fileDataBuffer := bytes.Buffer{}
	multipartWriter := multipart.NewWriter(&fileDataBuffer)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	formFile, err := multipartWriter.CreateFormFile(
		"myFile", file.Name(),
	)
	if err != nil {
		log.Fatal(err)
	}
	_, err = io.Copy(formFile, file)
	if err != nil {
		log.Fatal(err)
	}
	multipartWriter.Close()
	req, err := http.NewRequest(
		"POST",
		"http://localhost:8080",
		&fileDataBuffer,
	)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set(
		"Content-Type",
		multipartWriter.FormDataContentType(),
	)
	response, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	defer response.Body.Close()
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal(err)
	}
	return string(data)
}
