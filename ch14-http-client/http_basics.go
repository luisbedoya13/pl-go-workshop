package ch14

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type server struct{}

type messageData struct {
	Message string `json:"message"`
}

func GetWithHttpClient() string {
	r, err := http.Get("https://www.google.com")
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	return string(data)
}

func basicHttpClient() {
	data := GetWithHttpClient()
	text := []byte(data)
	err := ioutil.WriteFile("index.html", text, 0644)
	if err != nil {
		fmt.Println(err)
	}
	log.Println("Page stored successfully")
}

// such a middleware (?)
func (srv server) ServeHTTP(
	rs http.ResponseWriter, rq *http.Request,
) {
	msg := "{\"message\": \"hello world\"}"
	rs.Write([]byte(msg))
}

func BasicServer() {
	log.Fatal(http.ListenAndServe(":8080", server{}))
}

func GetJSONData() messageData {
	r, err := http.Get("http://localhost:8080")
	if err != nil {
		log.Fatal(err)
	}
	defer r.Body.Close()
	data, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}
	message := messageData{}
	err = json.Unmarshal(data, &message)
	if err != nil {
		log.Fatal(err)
	}
	return message
}
