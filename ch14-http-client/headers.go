package ch14

import (
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type server struct{}

func (srv server) ValidateHeaders(w http.ResponseWriter, r *http.Request) {
	auth := r.Header.Get("Authorization")
	if auth != "superSecretToken" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Authorization token not recognized"))
		return
	}
	time.Sleep(10 * time.Second)
	msg := "hello client!"
	w.Write([]byte(msg))
}

func GetDataWithCustomOptions() string {
	client := http.Client{Timeout: 11 * time.Second}
	// create the GET request
	req, err := http.NewRequest("POST", "http://localhost:8080", nil)
	if err != nil {
		log.Fatal(err)
	}
	req.Header.Set("Authorization", "superSecretToken")
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}
	// get data from the response body
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}
	// return the response data
	return string(data)
}
