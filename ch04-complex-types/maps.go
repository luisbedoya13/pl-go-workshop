package ch04

import "fmt"

var users = map[string]string{
	"305": "Sue",
	"204": "Bob",
	"631": "Jake",
	"073": "Tracy",
}

func Maps() {
	users := map[string]string{
		"305": "Sue",
		"204": "Bob",
		"631": "Jake",
	}
	users["073"] = "Tracy"
	fmt.Println(users)
}

func DeleteFromMap(id string) {
	delete(users, id)
}
