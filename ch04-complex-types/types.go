package ch04

import (
	"errors"
	"fmt"
	"math"
)

func ConvertNumbers() {
	var i8 int8 = math.MaxInt8
	i := 128
	f64 := 3.14
	fmt.Printf("int8    = %v  > in64    = %v\n", i8, int64(i8))
	fmt.Printf("int     = %v  > in8     = %v\n", i, int8(i))
	fmt.Printf("int8    = %v  > float32 = %v\n", i8, float64(i8))
	fmt.Printf("float64 = %v > int     = %v\n", f64, int(f64))
}

func TypeAssertion() {
	res, _ := doubler(5)
	fmt.Println("5   :", res)
	res, _ = doubler("yum")
	fmt.Println("yum :", res)
	_, err := doubler(true)
	fmt.Println("true:", err)
}

func doubler(v interface{}) (string, error) {
	if i, ok := v.(int); ok {
		return fmt.Sprint(i * 2), nil
	}
	if s, ok := v.(string); ok {
		return s + s, nil
	}
	return "", errors.New("unsupported type passed")
}

func TypeSwitch() {
	res, _ := doubler2(-5)
	fmt.Println("-5  :", res)
	res, _ = doubler2(5)
	fmt.Println("5   :", res)
	res, _ = doubler2("yum")
	fmt.Println("yum :", res)
	res, _ = doubler2(true)
	fmt.Println("true:", res)
	res, _ = doubler2(float32(3.14))
	fmt.Println("3.14:", res)
}

func doubler2(v interface{}) (string, error) {
	switch t := v.(type) {
	case string:
		return t + t, nil
	case bool:
		if t {
			return "truetrue", nil
		}
		return "falsefalse", nil
	case float32, float64:
		if f, ok := t.(float64); ok {
			return fmt.Sprint(f * 2), nil
		}
		return fmt.Sprint(t.(float32) * 2), nil
	case int:
		return fmt.Sprint(t * 2), nil
	case int8:
		return fmt.Sprint(t * 2), nil
	case int16:
		return fmt.Sprint(t * 2), nil
	case int32:
		return fmt.Sprint(t * 2), nil
	case int64:
		return fmt.Sprint(t * 2), nil
	case uint:
		return fmt.Sprint(t * 2), nil
	case uint8:
		return fmt.Sprint(t * 2), nil
	case uint16:
		return fmt.Sprint(t * 2), nil
	case uint32:
		return fmt.Sprint(t * 2), nil
	case uint64:
		return fmt.Sprint(t * 2), nil
	default:
		return "", errors.New("unsupported type passed")
	}
}
