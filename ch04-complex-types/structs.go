package ch04

import (
	"fmt"
)

type text string

type user struct {
	name    text
	age     int
	balance float64
	member  bool
}

var id1 text = "1234-5678"

func Struct() {
	u1 := user{
		name:    "Tracy",
		age:     51,
		balance: 98.43,
		member:  true,
	}
	u2 := user{
		age:  19,
		name: "Nick",
	}
	u3 := user{
		"Bob",
		25,
		0,
		false,
	}
	var u4 user
	u4.name = "Sue"
	u4.age = 31
	u4.member = true
	u4.balance = 17.09
	fmt.Println(u1, u2, u3, u4)
}

type name string

type location struct {
	x int
	y int
}

type size struct {
	width  int
	height int
}

type dot struct {
	name
	location
	size
}

func EmbedStruct() {
	dot1 := dot{
		name: "B",
		location: location{
			x: 13,
			y: 27,
		},
		size: size{
			width:  5,
			height: 7,
		},
	}
	fmt.Println(dot1)
}
