package ch13

import (
	"database/sql"
	"fmt"
)

// import _ "github.com/lib/pq"

func main() {
	db, err := sql.Open(
		"postgres",
		"user=postgres "+
			"password=postgres "+
			"host=127.0.0.1 "+
			"port=5432 "+
			"dbname=go_ws "+
			"sslmode=disable",
	)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("The connection to the DB was successfully initialized!")
	}
	connectivity := db.Ping()
	if connectivity != nil {
		panic(err)
	} else {
		fmt.Println("Good to go!")
	}
	DBCreate := `
		CREATE TABLE public.test (
			id integer,
			name character varying COLLATE pg_catalog."default"
		) WITH (
			OIDS = FALSE
		)
		TABLESPACE pg_default;
		ALTER TABLE public.test OWNER to postgres;
	`
	_, err = db.Exec(DBCreate)
	if err != nil {
		panic(err)
	} else {
		fmt.Println("The table was successfully created!")
	}
	insert, err := db.Prepare("INSERT INTO test VALUES ($1, $2)")
	if err != nil {
		panic(err)
	}
	_, err = insert.Exec(2, "second")
	if err != nil {
		panic(err)
	} else {
		fmt.Println("The value was successfully inserted!")
	}
	defer db.Close()
}
