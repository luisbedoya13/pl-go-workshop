package ch01

import (
	"fmt"
	"time"
)

var (
	Debug       bool      = false
	LogLevel    string    = "info"
	startUpTime time.Time = time.Now()
)

func VariableWithoutType() {
	Debug := false
	LogLevel := "info"
	startUpTime := time.Now()
	fmt.Println(Debug, LogLevel, startUpTime)
}

func MultipleAssign() {
	Debug, LogLevel, startUpTime := false, "info", time.Now()
	fmt.Println(Debug, LogLevel, startUpTime)
}

func Operators() {
	count := 5
	// Add to itself
	count += 5
	fmt.Println(count)
	// Increment by 1
	count++
	fmt.Println(count)
	// Decrement by 1
	count--
	fmt.Println(count)
	// Subtract from itself
	count -= 5
	fmt.Println(count)
	// This one works for strings
	name := "John"
	name += " Smith"
	fmt.Println("Hello,", name)
}

func ZeroValues() {
	var count int
	fmt.Printf("Count    : %#v \n", count)
	var discount float64
	fmt.Printf("Discount : %#v \n", discount)
	var debug bool
	fmt.Printf("Debug    : %#v \n", debug)
	var message string
	fmt.Printf("Message  : %#v \n", message)
	var emails []string
	fmt.Printf("Emails   : %#v \n", emails)
	var startTime time.Time
	fmt.Printf("Start    : %#v \n", startTime)
}
