package ch18

import (
	"crypto/md5"
	"crypto/sha256"
	"crypto/sha512"
	"fmt"
)

func GetHash(input string, hasType string) string {
	switch hasType {
	case "MD5":
		return fmt.Sprintf("%x", md5.Sum([]byte(input)))
	case "SHA256":
		return fmt.Sprintf("%x", sha256.Sum256([]byte(input)))
	case "SHA512":
		return fmt.Sprintf("%x", sha512.Sum512([]byte(input)))
	default:
		return fmt.Sprintf("%x", sha256.Sum256([]byte(input)))
	}
}
