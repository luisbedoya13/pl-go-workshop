package ch12

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"strings"
)

func CSV() {
	in := `firstName, lastName, age 
  Celina, Jones, 18 
  Cailyn, Henderson, 13 
  Cayden, Smith, 42 
  `
	r := csv.NewReader(strings.NewReader(in))
	records := false
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if records {
			for idx, value := range record {
				switch idx {
				case 0:
					fmt.Println("First Name: ", value)
				case 1:
					fmt.Println("Last Name: ", value)
				case 2:
					fmt.Println("Age: ", value)
				}
			}
		}
		records = true
	}
}
