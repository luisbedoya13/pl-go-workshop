package ch12

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
)

var (
	ErrWorkingFileNotFound = errors.New(
		"the working file is not found",
	)
)

func BackupFile() {
	backup := "backup.txt"
	workingFile := "note.txt"
	data := "note"
	err := createBackup(workingFile, backup)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for i := 0; i < 10; i++ {
		note := data + " " + strconv.Itoa(i)
		err := addNotes(workingFile, note)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}
}

func createBackup(working, backup string) error {
	_, err := os.Stat(working)
	if err != nil && os.IsNotExist(err) {
		return ErrWorkingFileNotFound
	} else if err != nil {
		return err
	}
	workFile, err := os.Open(working)
	if err != nil {
		return err
	}
	content, err := ioutil.ReadAll(workFile)
	err = ioutil.WriteFile(backup, content, 0644)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}

func addNotes(workingFile, notes string) error {
	notes += "\n"
	f, err := os.OpenFile(
		workingFile,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY,
		0644,
	)
	if err != nil {
		return err
	}
	defer f.Close()
	if _, err := f.Write([]byte(notes)); err != nil {
		return err
	}
	return nil
}
