package ch12

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func CreateFile() {
	f, err := os.Create("test.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()
	f.Write([]byte("Using Write function.\n"))
	f.WriteString("Using Writestring function.\n")
}

func CreateAndWrite() {
	message := []byte("Look!")
	err := ioutil.WriteFile("test.txt", message, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

func ReadFile() {
	content, err := ioutil.ReadFile("test.txt")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("File contents: ")
	fmt.Println(string(content))
}

func ReadFromReader() {
	f, err := os.Open("test.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	content, err := ioutil.ReadAll(f)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println("File contents: ")
	fmt.Println(string(content))
	r := strings.NewReader("No file here.")
	c, err := ioutil.ReadAll(r)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	fmt.Println()
	fmt.Println("Contents of strings.NewReader: ")
	fmt.Println(string(c))
}
