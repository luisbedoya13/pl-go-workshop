package ch16

import (
	"bytes"
	"log"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

func Coroutines() {
	var s1, s2 int
	go func() {
		s1 = sum(1, 100)
	}()
	s2 = sum(1, 10)
	time.Sleep(time.Second)
	log.Println(s1, s2)
}

func sum(from, to int) int {
	res := 0
	for i := from; i <= to; i++ {
		res += i
	}
	return res
}

func WaitGroup() {
	s1 := 0
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go sum2(1, 100, wg, &s1)
	wg.Wait()
	log.Println(s1)
}

func sum2(from, to int, wg *sync.WaitGroup, res *int) {
	*res = 0
	for i := from; i <= to; i++ {
		*res += i
	}
	wg.Done()
	return
}

func AtomicOp() {
	s1 := int32(0)
	wg := &sync.WaitGroup{}
	wg.Add(4)
	go sum3(1, 25, wg, &s1)
	go sum3(26, 50, wg, &s1)
	go sum3(51, 75, wg, &s1)
	go sum3(76, 100, wg, &s1)
	wg.Wait()
	log.Println(s1)
}

func sum3(from, to int, wg *sync.WaitGroup, res *int32) {
	for i := from; i <= to; i++ {
		atomic.AddInt32(res, int32(i))
	}
	wg.Done()
	return
}

func TestAtomicOp(t *testing.T) {
	for i := 0; i < 10000; i++ {
		var s bytes.Buffer
		log.SetOutput(&s)
		log.SetFlags(0)
		AtomicOp()
		if s.String() != "5050\n" {
			t.Error(s.String())
		}
	}
}
