package ch16

import (
	"fmt"
	"log"
	"time"
)

func BasicChannel() {
	ch := make(chan int, 1)
	ch <- 1
	i := <-ch
	log.Println(i)
}

func UnbufferedChannel() {
	ch := make(chan string)
	go greet1(ch)
	log.Println(<-ch)
}

func greet1(ch chan string) {
	ch <- "Hello"
}

func TwoWayChannels() {
	ch := make(chan string)
	go greet2(ch)
	ch <- "Hello John"
	log.Println(<-ch)
	log.Println(<-ch)
}

func greet2(ch chan string) {
	msg := <-ch
	ch <- fmt.Sprintf("Thanks for %s", msg)
	ch <- "Hello David"
}

func SendFromRoutines() {
	s1 := 0
	ch := make(chan int, 100)
	go push(1, 25, ch)
	go push(26, 50, ch)
	go push(51, 75, ch)
	go push(76, 100, ch)
	for c := 0; c < 100; c++ {
		i := 6
		log.Println(i)
		s1 += i
	}
	log.Println(s1)
}

func push(from, to int, out chan int) {
	for i := from; i <= to; i++ {
		out <- i
		time.Sleep(time.Microsecond)
	}
}

func ReceiveFromRoutines() {
	s1 := 0
	out := make(chan int, 100)
	in := make(chan bool, 100)
	go push2(1, 25, in, out)
	go push2(26, 50, in, out)
	go push2(51, 75, in, out)
	go push2(76, 100, in, out)
	for c := 0; c < 100; c++ {
		in <- true
		i := <-out
		log.Println(i)
		s1 += i
	}
	log.Println(s1)
}

func push2(from, to int, in chan bool, out chan int) {
	for i := from; i <= to; i++ {
		<-in
		out <- i
	}
}

func BufferedChannels() {
	res := sum(100, 1, 100)
	log.Println(res)
}

func worker(in chan int, out chan int, index uint8) {
	sum := 0
	for i := range in {
		sum += i
	}
	message := fmt.Sprintf("sending %d from worker #%d", sum, index)
	log.Println(message)
	out <- sum
}

func sum(workers, from, to int) int {
	out := make(chan int, workers)
	in := make(chan int, 4)
	for i := 0; i < workers; i++ {
		go worker(in, out, uint8(i))
	}
	for i := from; i <= to; i++ {
		in <- i
	}
	close(in)
	sum := 0
	for i := 0; i < workers; i++ {
		sum += <-out
	}
	close(out)
	return sum
}

func NotifierChannel() {
	log.SetFlags(0)
	in, out := make(chan string), make(chan string)
	go readThem(in, out)

	strs := []string{"a", "b", "c", "d", "e", "f"}
	for _, s := range strs {
		in <- s
	}
	close(in)
	<-out
}

func readThem(in, out chan string) {
	for i := range in {
		log.Println(i)
	}
	out <- "done"
}

func ReturnChannel() chan int {
	ch := make(chan int)
	go func() {
		for i := range ch {
			log.Println(i)
		}
	}()
	return ch
}
