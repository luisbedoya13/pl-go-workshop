package ch05

import "fmt"

func VariadicFunctions(nums ...int) int {
	total := 0
	for _, num := range nums {
		total += num
	}
	return total
}

func AnonymousFunctions() {
	j := 9
	x := func(i int) int {
		return i * i
	}
	fmt.Printf("The square of %d is %d\n", j, x(j))
}

func Closures() {
	counter := 4
	x := decrement(counter)
	fmt.Println(x())
	fmt.Println(x())
	fmt.Println(x())
	fmt.Println(x())
}

func decrement(i int) func() int {
	return func() int {
		i--
		return i
	}
}

func FunctionsType() {
	devSalary := salary(50, 2080, developerSalary)
	bossSalary := salary(150000, 25000, managerSalary)
	fmt.Printf("Boss salary: %d\n", bossSalary)
	fmt.Printf("Developer salary: %d\n", devSalary)
}

func salary(x, y int, f func(int, int) int) int {
	pay := f(x, y)
	return pay
}

func managerSalary(baseSalary, bonus int) int {
	return baseSalary + bonus
}

func developerSalary(hourlyRate, hoursWorked int) int {
	return hourlyRate * hoursWorked
}
