package ch10

import (
	"fmt"
	"strconv"
	"time"
)

func Formating() string {
	return time.Now().Format(time.ANSIC)
}

func TimeCalc() {
	Start := time.Now()
	time.Sleep(2 * time.Second)
	End := time.Now()
	fmt.Println(elapsedTime(Start, End))
}

func TimeZones() {
	fmt.Println(timeDiff("America/Los_Angeles"))
}

func elapsedTime(start time.Time, end time.Time) string {
	Elapsed := end.Sub(start)
	Hours := strconv.Itoa(int(Elapsed.Hours()))
	Minutes := strconv.Itoa(int(Elapsed.Minutes()))
	Seconds := strconv.Itoa(int(Elapsed.Seconds()))
	result := "The total execution time elapsed is: " + Hours
	result += " hour(s) and " + Minutes
	result += " minute(s) and " + Seconds + " second(s)!"
	return result
}

func timeDiff(timezone string) (string, string) {
	Current := time.Now()
	RemoteZone, _ := time.LoadLocation(timezone)
	RemoteTime := Current.In(RemoteZone)
	fmt.Println("The current time is: ", Current.Format(time.ANSIC))
	fmt.Println("The timezone:", timezone, "time is:", RemoteTime)
	return Current.Format(time.ANSIC), RemoteTime.Format(time.ANSIC)
}
