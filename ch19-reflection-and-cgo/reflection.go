package ch19

import (
	"fmt"
	"math"
	"reflect"
)

type circle struct {
	radius float64
}

type rectangle struct {
	length, breadth float64
}

func area(input interface{}) {
	inputType := reflect.TypeOf(input)
	fmt.Println("type is:", inputType.Name())
	if inputType.Name() == "circle" {
		val := reflect.ValueOf(input)
		radius := val.FieldByName("radius")
		fmt.Println("area is:", math.Pi*math.Pow(radius.Float(), 2))
	}
	if inputType.Name() == "rectangle" {
		val := reflect.ValueOf(input)
		length := val.FieldByName("length")
		breadth := val.FieldByName("breadth")
		fmt.Println("area is: ", length.Float()*breadth.Float())
	}
}

func Test() {
	area(circle{radius: 3})
	area(rectangle{length: 3.1, breadth: 7.2})
}

func DeepEqual() {
	runDeepEqual(nil, nil)
	runDeepEqual(make([]int, 10), make([]int, 10))
	runDeepEqual([3]int{1, 2, 3}, [3]int{1, 2, 3})
	runDeepEqual(map[int]string{1: "one", 2: "two"}, map[int]string{2: "two", 1: "one"})
}
func runDeepEqual(a, b interface{}) {
	fmt.Printf("%v DeepEqual %v : %v\n", a, b, reflect.DeepEqual(a, b))
}
