### build-tags:
> // +build linux
> // +build amd64,darwin 386,!gccgo
means ( amd64 AND darwin ) OR (386 AND (NOT gccgo))
> // +build darwin 
only builds in macOS
> // +build ignore
ignores the file

### Filenames:
> signal_darwin_amd64.go
> signal_darwin_arm.go
> signal_darwin_386.go
To utilize this method, the suffixes have to be of the following form:
> *_GOOS
> *_GOARCH
> *_GOOS_GOARCH