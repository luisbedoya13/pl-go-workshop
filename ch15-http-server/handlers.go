package ch15

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
)

type ServerHandler struct{}

type Request struct {
	Name    string
	Surname string
}

type Response struct {
	Greeting string
}

func (h ServerHandler) ServeHTTP(
	rs http.ResponseWriter, rq *http.Request,
) {
	msg := "<h1>Hello world</h1>"
	rs.Write([]byte(msg))
}

func HandlerFunction(rs http.ResponseWriter, rq *http.Request) {
	msg := "<h1>Bonjour monde</h1>"
	rs.Write([]byte(msg))
}

func GreetingHandler(rs http.ResponseWriter, rq *http.Request) {
	qs := rq.URL.Query()
	name, exists := qs["name"]
	if !exists {
		rs.WriteHeader(400)
		rs.Write([]byte("Missing name"))
		return
	}
	rs.Write([]byte(
		fmt.Sprintf("Hello %s", strings.Join(name, ","))),
	)
}

func StaticFilesHandler(rw http.ResponseWriter, r *http.Request) {
	http.ServeFile(rw, r, "./public/index.html")
}

func JSONGreeting(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var data Request
	err := decoder.Decode(&data)
	if err != nil {
		w.WriteHeader(400)
		return
	}
	res := Response{
		Greeting: fmt.Sprintf("Hello %s %s", data.Name, data.Surname),
	}
	bts, err := json.Marshal(res)
	if err != nil {
		w.WriteHeader(400)
		return
	}
	w.Write(bts)
}

func Server() {
	http.HandleFunc("/es", func(
		rs http.ResponseWriter, rq *http.Request,
	) {
		msg := "<h1>Hola mundo</h1>"
		rs.Write([]byte(msg))
	})
	http.HandleFunc("/fr", HandlerFunction)
	http.HandleFunc("/greeting", GreetingHandler)
	http.HandleFunc("/json-greeting", JSONGreeting)
	http.HandleFunc("/static", StaticFilesHandler)
	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("./public")),
		),
	)
	http.Handle("/", ServerHandler{})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
