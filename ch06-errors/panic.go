package ch06

import (
	"errors"
	"fmt"
)

var (
	ErrHourlyRate = errors.New("invalid hourly rate")
)

func Panic() {
	pay := payDay2(8, 50)
	fmt.Println(pay)
}

func Recover() {
	pay := payDay3(8, 100)
	fmt.Println(pay)
}

func payDay2(hoursWorked, hourlyRate int) int {
	report := func() {
		fmt.Printf(
			"HoursWorked: %d\nHourlyRate: %d\n",
			hoursWorked,
			hourlyRate,
		)
	}
	defer report()
	if hourlyRate < 5 || hourlyRate > 30 {
		panic(ErrHourlyRate)
	}
	return hoursWorked * hourlyRate
}

func payDay3(hoursWorked, hourlyRate int) int {
	defer func() {
		if r := recover(); r != nil {
			if r == ErrHourlyRate {
				fmt.Printf(
					"hourly rate: %d\nerr: %v\n\n",
					hourlyRate,
					r,
				)
			}
		}
		fmt.Printf(
			"HoursWorked: %d\nHourlyRate: %d\n",
			hoursWorked,
			hourlyRate,
		)
	}()
	if hourlyRate < 5 || hourlyRate > 30 {
		panic(ErrHourlyRate)
	}
	if hoursWorked > 8 {
		hoursOver := hoursWorked - 8
		overTime := hoursOver * 2
		regularPay := hoursWorked * hourlyRate
		return regularPay + overTime
	}
	return hoursWorked * hourlyRate
}
