package ch06

import (
	"errors"
	"fmt"
)

var (
	ErrHourlyRate = errors.New("invalid hourly rate")
)

func ErrorValues() {
	_, err := payDay(10, 50)
	if err != nil {
		fmt.Println(err)
	}
}

func payDay(hoursWorked, hourlyRate int) (int, error) {
	if hourlyRate < 5 || hourlyRate > 30 {
		return 0, ErrHourlyRate
	}
	if hoursWorked > 8 {
		hoursOver := hoursWorked - 8
		overTime := hoursOver * 2
		regularPay := hoursWorked * hourlyRate
		return regularPay + overTime, nil
	}
	return hoursWorked * hourlyRate, nil
}
